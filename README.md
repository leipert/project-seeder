# Project Seeder

This node script will export an existing project on GitLab and import it into a new namespace.

Optionally:
- you can also add a user into the new project (`--add-username "username"`)
- you can also add an expiration to the new user. It should follow the `YYYY-MM-DD` format (`--add-user-expiration "YYYY-MM-DD"`)
- auto trigger a pipeline for a specific branch (`--run-branch-pipeline "branch-name"`)

## Program Parameters

To view the program parameters, run `node index.js --help`

## Running this program (Locally)

1. `yarn install`
1. `cp .env.example.sh .env.sh` and edit the example file
1. edit and run `./run.sh`
