const path = require('path');
const program = require('commander');

const { EXPORT_WAIT_TIMEOUT } = require('./scripts/constants');
const { logError, logInfo, log } = require('./scripts/log');
const api = require('./scripts/api');
const importApi = require('./scripts/import_project');
const exportApi = require('./scripts/export_project');
const userApi = require('./scripts/user');
const pipelineApi = require('./scripts/pipeline');
const issueApi = require('./scripts/issue');
const mergeRequestApi = require('./scripts/merge_request');
const projectApi = require('./scripts/project');

const API_URL = 'https://gitlab.com/api/v4';
const WEB_URL = 'https://gitlab.com/';

program
  .version('1.0.0')
  .requiredOption('-t, --token <token>', 'GitLab private token (Required)')
  .requiredOption(
    '-pi, --project-id <project-id>',
    'Project ID of original project (Required)'
  )
  .requiredOption(
    '-npn, --new-project-name <new-project-name>',
    'Name of new project (Required)'
  )
  .requiredOption(
    '-nnn, --new-namespace-name <new-namespace-name>',
    'Namespace of new project (Required)'
  )
  .option(
    '-au, --add-username <username>',
    'Username to add to new project (Optional)'
  )
  .option(
    '-aue, --add-user-expiration <YYYY-MM-DD>',
    'Expiration date of new user that was added to project (Optional)'
  )
  .option(
    '--assign-user-to-issue <iid>',
    'Assign issue with iid to the new user that was added to project (Optional)'
  )
  .option(
    '--assign-user-to-mr <iid>',
    'Assign MR with iid to the new user that was added to project (Optional)'
  )
  .option(
    '-rbp, --run-branch-pipeline <branch-name>',
    'Run pipeline for branch (Optional)'
  )
  .parse(process.argv);

async function main(program) {
  const exportFilePath = path.resolve(__dirname, 'export.tar.gz');

  const {
    token,
    projectId,
    newNamespaceName,
    newProjectName,
    addUsername: usernameToAdd,
    addUserExpiration,
    assignUserToIssue,
    assignUserToMr,
    runBranchPipeline: branchPipeline,
  } = program;

  logInfo(
    `Importing project (${projectId}) to ${newNamespaceName}/${newProjectName}`
  );

  api.setup(API_URL, token, WEB_URL);

  let newProjectId = await projectApi.getProjectID(
    newNamespaceName,
    newProjectName
  );

  if (!newProjectId) {
    await exportApi.createAndDownloadExport(projectId, exportFilePath);

    // Wait between download and import to reduce intermittent API failures
    await new Promise(resolve => setTimeout(resolve, EXPORT_WAIT_TIMEOUT));

    const project = await importApi.importProject(
      newNamespaceName,
      newProjectName,
      exportFilePath
    );

    newProjectId = project.id;

    await importApi.waitForImportFinish(newProjectId);
  }

  if (usernameToAdd) {
    logInfo(`Trying to retrieve the user id for: @${usernameToAdd}`);

    const userId = await userApi.getUserId(usernameToAdd);

    logInfo(`\t@${usernameToAdd} has the id ${userId}`);

    const consoleMessage = `Adding user @${usernameToAdd} to the new project`;

    if (addUserExpiration) {
      logInfo(`${consoleMessage} with an expiration of ${addUserExpiration}`);
    } else {
      logInfo(`${consoleMessage} with no expiration`);
    }

    const { data } = await userApi.addUserToProject(
      userId,
      newProjectId,
      addUserExpiration
    );

    logInfo(
      `\tSuccessfully added @${data.username} (${data.name}) to the project`
    );

    if (assignUserToIssue) {
      logInfo(`Assigning to issue #${assignUserToIssue}`);
      const { data } = await issueApi.assignIssue(
        newProjectId,
        assignUserToIssue,
        userId
      );
      logInfo(
        `\tSuccessfully Assigned @${usernameToAdd} to issue #${assignUserToIssue}: ${data.web_url}`
      );
    }

    if (assignUserToMr) {
      logInfo(`Assigning to MR #${assignUserToMr}`);
      const { data } = await mergeRequestApi.assignMergeRequest(
        newProjectId,
        assignUserToMr,
        userId
      );
      logInfo(
        `\tAssigned @${usernameToAdd} to MR !${assignUserToMr}: ${data.web_url}`
      );
    }
  }

  if (branchPipeline) {
    logInfo(`Triggering pipeline for ${branchPipeline} in the new project`);

    const { data } = await pipelineApi.runPipeline(
      newProjectId,
      branchPipeline
    );
    logInfo(`\tStarted pipeline: ${data.web_url}`);
  }
}

main(program).catch(error => {
  console.warn(error.response);
  logError(error);
  process.exit(1);
});
