source .env.sh

function import_project {
node index.js \
  --token "$GITLAB_TOKEN" \
  --project-id "13003231" \
  --new-project-name "$1" \
  --new-namespace-name "$TARGET_GROUP_ID" \
  --run-branch-pipeline 1-feature \
  --assign-user-to-issue "1" \
  --assign-user-to-mr "3" \
  --add-username "$2" \
  --add-user-expiration "$3"
}

# import_project "applicant_id" "username" "interview-date"
