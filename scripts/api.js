const path = require('path');
const axios = require('axios');

let gitLabAPI = null;
let gitLabWebUrl = null;

const setup = (baseURL, token, webUrl) => {
  gitLabAPI = axios.create({
    baseURL: baseURL,
    headers: { 'PRIVATE-TOKEN': token },
  });
  gitLabWebUrl = webUrl;
};

const handler = {
  get: function(obj, prop) {
    if (prop === 'setup') {
      return setup;
    }

    if (gitLabWebUrl && prop === 'buildWebUrl') {
      return relPath => path.join(gitLabWebUrl, relPath);
    }

    if (gitLabAPI) {
      return gitLabAPI[prop];
    }

    throw new Error('Please initialize the API by calling .setup');
  },
};

module.exports = new Proxy({}, handler);
