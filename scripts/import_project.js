const fs = require('fs');
const FormData = require('form-data');

const gitLab = require('./api');
const utils = require('./utils');
const { logInfo } = require('./log');

async function importProject(namespace, path, filePath) {
  logInfo('Starting import');

  const buffer = fs.createReadStream(filePath);

  const formData = new FormData();
  formData.append('path', path);
  formData.append('namespace', namespace);
  formData.append('file', buffer);

  const { data } = await gitLab.post(`/projects/import`, formData, {
    headers: {
      ...formData.getHeaders(),
    },
  });

  return data;
}

async function checkImportStatus(projectId) {
  const { data, headers } = await gitLab.get(`/projects/${projectId}/import`);

  const status = data.import_status;

  if (status === 'failed') {
    let message = `Import failed with this response: ${JSON.stringify(
      data,
      null,
      2
    )}`;
    message += `\n\nHowever, please check if the project has been created under:`;
    message += `\n\t${gitLab.buildWebUrl(data.path_with_namespace)}`;
    message += `\n\nIf the project has been created successfully, you can re-run the script to add users, etc.`;
    throw new Error(message);
  }

  return status;
}

async function waitForImportFinish(projectId) {
  logInfo('Waiting for import to finish:');

  await utils.waitForImportExportToFinish(checkImportStatus, projectId);

  return projectId;
}

module.exports = {
  importProject,
  waitForImportFinish,
};
