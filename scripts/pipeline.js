const gitLab = require('./api');

function runPipeline(projectId, branchName) {
  return gitLab.post(`/projects/${projectId}/pipeline?ref=${branchName}`, {});
}

module.exports = {
  runPipeline,
};
