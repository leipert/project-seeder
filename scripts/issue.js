const gitLab = require('./api');

function assignIssue(projectId, issueIid, userId) {
  return gitLab.put(`/projects/${projectId}/issues/${issueIid}`, { assignee_ids: [userId] });
}

module.exports = {
  assignIssue,
};
