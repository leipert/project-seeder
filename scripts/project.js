const gitLab = require('./api');

async function getProjectID(groupId, projectName) {
  const { data } = await gitLab.get(`/groups/${groupId}`);
  const path = `${data.full_path}/${projectName}`;
  try {
    const { data: project } = await gitLab.get(`/projects/${encodeURIComponent(path)}`)

    if (project && project.id) {
      return project.id
    }

  } catch {}
  return false;
}

module.exports = {
  getProjectID,
};
