const gitLab = require('./api');

function assignMergeRequest(projectId, mrIid, userId) {
  return gitLab.put(`/projects/${projectId}/merge_requests/${mrIid}`, { assignee_ids: [userId] });
}

module.exports = {
  assignMergeRequest,
};
