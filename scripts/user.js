const gitLab = require('./api');

async function getUserId(username) {
  let data;
  try {
    data = (await gitLab.get(`/users?username=${username}`)).data;
  } catch (e) {
    throw new Error(`Could not get id for user @${username}:\n${e.message}`);
  }

  if (data.length === 0) {
    throw new Error(`No user found with name @${username}`);
  }

  return data[0].id;
}

async function addUserToProject(
  userId,
  newProjectId,
  expiresAt = null,
  accessLevel = 30
) {
  // Access levels
  // 10 => Guest access
  // 20 => Reporter access
  // 30 => Developer access
  // 40 => Maintainer access
  // 50 => Owner access # Only valid for groups

  let existingUser = false;

  try {
    existingUser = await gitLab.get(
      `/projects/${newProjectId}/members/${userId}`
    );
  } catch (e) {
    if (e.response.status !== 404) {
      throw new Error(`Could not get project member ${userId}: ${e.message}`);
    }
  }

  // User exists and everything is okay
  if (
    existingUser &&
    existingUser.data &&
    existingUser.data.id === userId &&
    existingUser.data.access_level === accessLevel &&
    existingUser.data.expires_at === expiresAt
  ) {
    return existingUser;
  }

  const payload = {
    access_level: accessLevel,
    expires_at: expiresAt,
  };

  try {
    // Update user
    if (existingUser) {
      return await gitLab.put(
        `/projects/${newProjectId}/members/${userId}`,
        payload
      );
    }

    // Add user
    return await gitLab.post(`/projects/${newProjectId}/members`, {
      user_id: userId,
      ...payload,
    });
  } catch (e) {
    throw new Error(`Could not add user to project: ${e.message}`);
  }
}

module.exports = {
  getUserId,
  addUserToProject,
};
