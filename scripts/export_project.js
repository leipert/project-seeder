const fs = require('fs');

const gitLab = require('./api');
const utils = require('./utils');
const { logInfo } = require('./log');

function requestExportDownload(projectId) {
  return gitLab.post(`/projects/${projectId}/export`, {});
}

async function checkExportStatus(projectId) {
  const { data } = await gitLab.get(`/projects/${projectId}/export`);

  const status = data.export_status;

  if (status === 'failed') {
    throw new Error('Export failed with this response: ' + JSON.stringify(data, null, 2));
  }

  return status;
}

async function waitForExportFinish(projectId) {
  logInfo('Waiting for export to finish:');

  await utils.waitForImportExportToFinish(checkExportStatus, projectId);
}

async function downloadExport(projectId, filePath) {
  const { data } = await gitLab.get(`/projects/${projectId}/export/download`, {
    responseType: 'stream'
  });

  return new Promise((resolve, reject) => {
    const w = fs.createWriteStream(filePath);

    data.pipe(w);

    w.on('finish', () => {
      logInfo(`Successfully downloaded export to ${filePath}`);
      resolve();
    });
    w.on('error', (e) => reject(e));
  });
}

async function createAndDownloadExport(projectId, filePath, maxAgeInHours = 12) {

  try {
    const { headers } = await gitLab.head(`/projects/${projectId}/export/download`);
    const lastModified = new Date(headers['last-modified']);
    if ((new Date() - lastModified) < maxAgeInHours * 60 * 60 * 1000) {
      logInfo('Last export was created less than 12h ago');
      return downloadExport(projectId, filePath);
    }
  } catch (e) {
    // In case we do not have an export
    console.warn(e);
  }

  logInfo('No export found, creating one');

  await requestExportDownload(projectId);
  await waitForExportFinish(projectId);
  return downloadExport(projectId, filePath);
}

module.exports = {
  createAndDownloadExport,
};
